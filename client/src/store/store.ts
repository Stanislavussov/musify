import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { playerReducer } from "./redusers/playerReducer";
import { trackReducer } from "./redusers/trackReducer";

const rootReducer = combineReducers({
  player: playerReducer,
  track: trackReducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
  });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore["dispatch"];
