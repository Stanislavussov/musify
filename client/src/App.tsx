import * as React from "react";
import "./styles/app.css";
import {
  BrowserRouter as Router,
  Navigate,
  Route,
  Routes,
  useLocation,
} from "react-router-dom";
import Home from "./Home";
import TrackList from "./views/track/TrackList";
import Navbar from "./components/Navbar";
import TrackDetail from "./views/track/TrackDetail";
import CreateTrack from "./views/track/CreateTrack";
import Player from "./components/Player";

type RoutesType = {
  path: string;
  exact: boolean;
  element: JSX.Element;
  child?: {
    path: string;
    exact: boolean;
    element: JSX.Element;
  };
}[];

/** App routes definition */
const appRoutes = (): RoutesType => {
  return [
    {
      path: "/",
      exact: true,
      element: <Home />,
    },
    {
      path: "/tracks",
      exact: true,
      element: <TrackList />,
      child: {
        path: "/tracks/:id(\\d+)",
        element: <TrackDetail />,
        exact: true,
      },
    },
    {
      path: "/tracks/:id",
      exact: true,
      element: <TrackDetail />,
    },
    {
      path: "/tracks/create",
      exact: true,
      element: <CreateTrack />,
    },
    { path: "*", exact: true, element: <Navigate to="/" /> },
  ];
};

/** Route list component
 * TODO: Possible to add permissions (private/public)
 * */
const RouteList = (): JSX.Element => {
  const location = useLocation();

  return (
    <Routes>
      {appRoutes().map(({ path, element, child }, routeIndex) => (
        <Route
          key={routeIndex}
          path={path}
          element={element}
          children={<Route element={child?.element} path={child?.path} />}
        />
      ))}
    </Routes>
  );
};

/** App component */
const App: React.FC = () => {
  return (
    <>
      <Router>
        <Navbar />
        <div className="container mx-auto px-4">
          <RouteList />
        </div>
        <Player />
      </Router>
    </>
  );
};

export default App;
