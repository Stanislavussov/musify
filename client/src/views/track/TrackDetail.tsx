import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  Textarea,
} from "@chakra-ui/react";
import { Field, Form, Formik } from "formik";
import React from "react";
import { useNavigate } from "react-router-dom";

const TrackDetail: React.FC = () => {
  // const location = useLocation();
  const history = useNavigate();

  const track = {
    _id: "1",
    artist: "Artist",
    audio: "track",
    listens: 22012,
    comments: [
      {
        _id: "1",
        text: "Comment",
        username: "UserName",
      },
    ],
    text: "Shine bright like a diamond",
    name: "Name",
    picture:
      "https://lh3.googleusercontent.com/tSXNsRNWx0mDryn5E7NLnBqF-r9XOrXQjdMP2QeECpPk8Z8U6xgUDjlgB9WMwUbDEcIUFcVjgmIyGfU=w544-h544-l90-rj",
  };

  return (
    <div>
      <Button
        onClick={() => {
          history("/tracks");
        }}
      >
        Back to track list
      </Button>
      <div className="track-detail__box">
        <img className="w-96 h-96" src={track.picture} alt={"trackPicture"} />
        <div className={"w-full"}>
          <div className="pb-8">
            <div className={"track-detail__title_track"}>{track.name}</div>
            <div className={"track-track-detail__title_artist"}>
              {track.artist}
            </div>
            <div className={"track-detail__listens"}>
              {track.listens} listens
            </div>
          </div>
          <Tabs>
            <TabList>
              <Tab>Lyrics</Tab>
              <Tab>Comments</Tab>
            </TabList>

            <TabPanels>
              <TabPanel>
                <div className={"overflow-y-scroll h-60"}>{track.text}</div>
              </TabPanel>
              <TabPanel>
                <div className={"w-full"}>
                  <Formik
                    initialValues={{ name: "" }}
                    onSubmit={(values, actions) => {
                      setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        actions.setSubmitting(false);
                      }, 1000);
                    }}
                  >
                    {(props) => (
                      <Form>
                        <Field name="name">
                          {({ field, form }: any) => (
                            <FormControl
                              className={"py-4"}
                              isInvalid={form.errors.name && form.touched.name}
                            >
                              <FormLabel htmlFor="name">Name</FormLabel>
                              <Input
                                required={true}
                                {...field}
                                id="name"
                                placeholder="name"
                              />
                              <FormErrorMessage>
                                {form.errors.name}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                        <Field name="comment">
                          {({ field, form }: any) => (
                            <FormControl
                              className={"py-4"}
                              isInvalid={form.errors.name && form.touched.name}
                            >
                              <FormLabel htmlFor="comment">Comment</FormLabel>
                              <Textarea
                                required={true}
                                {...field}
                                id="comment"
                                placeholder="Comment goes here..."
                              />
                              <FormErrorMessage>
                                {form.errors.comment}
                              </FormErrorMessage>
                            </FormControl>
                          )}
                        </Field>
                        <Button
                          mt={4}
                          colorScheme="teal"
                          isLoading={props.isSubmitting}
                          type="submit"
                        >
                          Leave comment
                        </Button>
                      </Form>
                    )}
                  </Formik>
                </div>
              </TabPanel>
            </TabPanels>
          </Tabs>
        </div>
      </div>
    </div>
  );
};

export default TrackDetail;
