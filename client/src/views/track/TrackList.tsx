import React from "react";
import TrackItem from "./TrackItem";
import { Button } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import { useAppSelector } from "../../hooks/useAppDispatch";

// type TrackListStateType = {
//   tracks: ITrack[];
// };

const TrackList: React.FC = () => {
  const history = useNavigate();
  const { tracks, error } = useAppSelector((state) => state.track);

  if (error) {
    return (
      <div className="container mx-auto px-4">
        <h1>{error}</h1>
      </div>
    );
  }

  return (
    <div className={"relative"}>
      <div className={"absolute -top-20 right-0"}>
        <Button
          onClick={() => {
            history("/tracks/create");
          }}
          variant={"solid"}
          colorScheme={"teal"}
        >
          Upload
        </Button>
      </div>
      {tracks
        ? tracks.map((track) => <TrackItem key={track._id} track={track} />)
        : []}
    </div>
  );
};

export default TrackList;
