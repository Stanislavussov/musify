import React from "react";
import { ITrack } from "../../types/trackType";
import { IconButton, useDisclosure } from "@chakra-ui/react";
import { AiOutlinePauseCircle, AiOutlinePlayCircle } from "react-icons/ai";
import { BsTrashFill } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import ModalWindow from "../../components/ModalWindow";
import { useAppDispatch } from "../../hooks/useAppDispatch";
import { PlayerActionTypes } from "../../types/playerStateType";

interface TrackItemProps {
  track: ITrack;
  active?: boolean;
}

const TrackItem: React.FC<TrackItemProps> = ({ track, active = false }) => {
  const history = useNavigate();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const dipatch = useAppDispatch();

  const playHandler = (e: React.ChangeEvent<any>) => {
    e.stopPropagation();
    dipatch({
      type: PlayerActionTypes.SET_ACTIVE,
      payload: track,
    });
    dipatch({
      type: PlayerActionTypes.PLAY,
    });
  };

  return (
    <div
      className="track-box"
      onClick={(e) => {
        e.stopPropagation();
        history(track._id);
      }}
    >
      <div className="flex gap-10 items-center">
        <IconButton
          aria-label={"play"}
          icon={
            active ? (
              <AiOutlinePauseCircle size="22px" />
            ) : (
              <AiOutlinePlayCircle size="22px" />
            )
          }
          size="lg"
          onClick={playHandler}
        />
        <img src={track.picture} className="track-img" alt="cover" />
      </div>
      <div>
        <div>
          <b>{track.name}</b>
        </div>
        <div>{track.artist}</div>
      </div>
      <IconButton
        aria-label={"delete"}
        icon={<BsTrashFill size="19px" />}
        size="lg"
        variant={"ghost"}
        onClick={(e) => {
          e.stopPropagation();
          onOpen();
        }}
        type={"button"}
      />
      <div className="track-time">{active ? "01:20 / 03:31" : null}</div>
      <ModalWindow
        onClose={onClose}
        isOpen={isOpen}
        windowContent={{
          title: "Delete track",
          body: "Do you really want to remove this track?",
          closeButton: "Back",
          successButton: "Delete",
        }}
      />
    </div>
  );
};

export default TrackItem;
