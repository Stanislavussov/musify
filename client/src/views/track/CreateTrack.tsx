import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Textarea,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { Field, Form, Formik } from "formik";
import FileUpload from "../../components/FileUpload";

interface ICreateTrackState {
  activeStep: number;
  picture: null | File;
  track: null | File;
}

const CreateTrack: React.FC = () => {
  const [state, setState] = useState<ICreateTrackState>({
    activeStep: 0,
    picture: null,
    track: null,
  });

  return (
    <div>
      <UploadProgress activeStep={state.activeStep} />
      <StepWrapper activeStep={state.activeStep}>
        <div className="track-upload-box">
          {uploadComponentList({
            state,
            setState,
          }).map(({ component }, componentId) => (
            <div key={componentId}>
              {componentId === state.activeStep ? component : null}
            </div>
          ))}
        </div>
      </StepWrapper>
    </div>
  );
};

interface StepProps {
  state: ICreateTrackState;
  setState: React.Dispatch<React.SetStateAction<ICreateTrackState>>;
}

const Step1: React.FC<StepProps> = ({ state, setState }) => {
  return (
    <div>
      <Formik
        initialValues={{ name: "" }}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props) => (
          <Form>
            <Field name="name">
              {({ field, form }: any) => (
                <FormControl
                  className={"py-4"}
                  isInvalid={form.errors.name && form.touched.name}
                >
                  <FormLabel htmlFor="name">Name</FormLabel>
                  <Input
                    required={true}
                    {...field}
                    id="name"
                    placeholder="name"
                  />
                  <FormErrorMessage>{form.errors.name}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
            <Field name="comment">
              {({ field, form }: any) => (
                <FormControl
                  className={"py-4"}
                  isInvalid={form.errors.name && form.touched.name}
                >
                  <FormLabel htmlFor="comment">Comment</FormLabel>
                  <Textarea
                    required={true}
                    {...field}
                    id="comment"
                    placeholder="Comment goes here..."
                  />
                  <FormErrorMessage>{form.errors.comment}</FormErrorMessage>
                </FormControl>
              )}
            </Field>
          </Form>
        )}
      </Formik>
      <Button
        onClick={() => {
          setState({
            ...state,
            activeStep: state.activeStep + 1,
          });
        }}
      >
        GO
      </Button>
    </div>
  );
};

const Step2: React.FC<StepProps> = ({ state, setState }) => {
  return (
    <div>
      <FileUpload
        setFile={(picture) => {
          setState({
            ...state,
            picture: picture,
            activeStep: state.activeStep + 1,
          });
        }}
        accept={"image/*"}
      >
        <Button>Upload image</Button>
      </FileUpload>
      {/*<Button*/}
      {/*  onClick={() => {*/}
      {/*    setState({*/}
      {/*      ...state,*/}
      {/*      activeStep: state.activeStep + 1,*/}
      {/*    });*/}
      {/*  }}*/}
      {/*>*/}
      {/*  GO*/}
      {/*</Button>*/}
    </div>
  );
};

const Step3: React.FC<StepProps> = ({ state, setState }) => {
  return (
    <div>
      <FileUpload
        setFile={(track) => {
          setState({
            ...state,
            track: track,
          });
        }}
        accept={"audio/*"}
      >
        <Button>Upload track</Button>
      </FileUpload>
      <Button
        onClick={() => {
          setState({
            ...state,
            // activeStep: state.activeStep + 1,
          });
        }}
      >
        GO
      </Button>
    </div>
  );
};

const uploadComponentList = (props: {
  state: ICreateTrackState;
  setState: React.Dispatch<React.SetStateAction<ICreateTrackState>>;
}): { component: JSX.Element }[] => {
  return [
    { component: <Step1 {...props} /> },
    { component: <Step2 {...props} /> },
    { component: <Step3 {...props} /> },
  ];
};

interface IUploadProgress {
  activeStep: number;
}

const steps: string[] = ["Info about track", "Upload image", "Upload track"];

const UploadProgressItem = ({ step, stepId, active }: any) => {
  return (
    <div className={"flex gap-4"}>
      <span
        className={
          active
            ? "create-track__upload_progress_item_active"
            : "create-track__upload_progress_item"
        }
      >
        {stepId + 1}
      </span>
      <span className={`text-2xl ${active && "font-bold"}`}>{step}</span>
    </div>
  );
};

const UploadProgress: React.FC<IUploadProgress> = ({ activeStep }) => {
  return (
    <div className="flex justify-center gap-20">
      {steps.map((step, stepId) =>
        stepId === activeStep ? (
          <UploadProgressItem
            key={stepId}
            stepId={stepId}
            step={step}
            active={true}
          />
        ) : (
          <UploadProgressItem
            key={stepId}
            stepId={stepId}
            step={step}
            active={false}
          />
        )
      )}
    </div>
  );
};

interface IStepWrapper {
  activeStep: number;
  children: React.ReactNode;
}

const StepWrapper: React.FC<IStepWrapper> = ({ activeStep, children }) => {
  return (
    <div>
      {steps.map((step, stepId) =>
        stepId === activeStep ? <div key={stepId}>{children}</div> : null
      )}
    </div>
  );
};

export default CreateTrack;
