import { useState, useEffect } from "react";
import axios, { AxiosRequestConfig } from "axios";

axios.defaults.baseURL = "http://localhost:5000";

/** Custom hook for data fetching */
const useDataProvider = <R, E>({
  url,
  method = "GET",
  headers,
  data,
}: AxiosRequestConfig): {
  response: R | undefined;
  error: E | undefined;
  loading: boolean;
  fetchData: (config?: AxiosRequestConfig) => void;
} => {
  const [response, setResponse] = useState<R | undefined>(undefined);
  const [error, setError] = useState<E | undefined>(undefined);
  const [loading, setLoading] = useState(false);

  const fetchData = (config?: AxiosRequestConfig) => {
    setLoading(true);
    axios(
      config || {
        method: method,
        url: url,
        data: data,
        headers: headers,
      }
    )
      .then((res: { data: R | undefined }) => {
        setResponse(res.data);
      })
      .catch((err: E | undefined) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (url) {
      fetchData();
    }
  }, [method, url, headers]);

  return { response, error, loading, fetchData };
};

export default useDataProvider;
