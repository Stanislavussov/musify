import React from "react";
// import "./index.css";

const Home = () => {
  return (
    <div className={"centered"}>
      <div className="h1">Welcome to Amazing Musify APP</div>
      <div className="h2">Here you can find all your favorite tracks</div>
    </div>
  );
};

export default Home;
