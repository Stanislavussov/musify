import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import React from "react";

interface ModalWindowProps {
  isOpen: boolean;
  onClose: () => void;
  windowContent: {
    title: string;
    body: string;
    closeButton: string;
    successButton: string;
  };
}

const ModalWindow: React.FC<ModalWindowProps> = ({
  isOpen,
  onClose,
  windowContent,
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{windowContent.title}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>{windowContent.body}</ModalBody>
        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={onClose}>
            {windowContent.closeButton}
          </Button>
          <Button variant="ghost">{windowContent.successButton}</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default ModalWindow;
