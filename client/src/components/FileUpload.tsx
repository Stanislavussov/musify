import React, { useRef } from "react";

interface FileUploadProps {
  setFile: (p: File) => void;
  accept: string;
}

/** Component for file uploads */
const FileUpload: React.FC<FileUploadProps> = ({
  setFile,
  accept,
  children,
}) => {
  const ref = useRef<HTMLInputElement>(null);

  const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setFile(e.target.files[0]);
    }
  };

  return (
    <div
      onClick={() => {
        ref.current && ref.current.click();
      }}
    >
      {/** Hide original input to use custom child component  */}
      <input
        type="file"
        accept={accept}
        style={{ display: "none" }}
        ref={ref}
        onChange={onChangeHandler}
      />
      {children}
    </div>
  );
};

export default FileUpload;
