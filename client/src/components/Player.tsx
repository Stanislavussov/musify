import React from "react";
import { IconButton } from "@chakra-ui/react";
import {
  AiOutlinePauseCircle,
  AiOutlinePlayCircle,
  AiFillSound,
} from "react-icons/ai";
import TrackProgress from "./TrackProgress";
import { useAppDispatch, useAppSelector } from "../hooks/useAppDispatch";
import { PlayerActionTypes } from "../types/playerStateType";
import { useEffect } from "react";

let audio = new Audio();

const Player = () => {
  const track = {
    _id: "1",
    artist: "Artist",
    audio: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
    listens: 22012,
    comments: [
      {
        _id: "1",
        text: "Comment",
        username: "UserName",
      },
    ],
    text: "\n" + "Shine bright like a diamond",
    name: "Song name 123",
    picture:
      "https://lh3.googleusercontent.com/tSXNsRNWx0mDryn5E7NLnBqF-r9XOrXQjdMP2QeECpPk8Z8U6xgUDjlgB9WMwUbDEcIUFcVjgmIyGfU=w544-h544-l90-rj",
  };

  const { volume, active, duration, currentTime, pause } = useAppSelector(
    (state) => state.player
  );

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!audio) {
      audio = new Audio();
    } else {
      setAudio();
      dispatch({
        type: PlayerActionTypes.PLAY,
      });
    }
  }, [active]);

  const setAudio = () => {
    audio.src = track.audio;
    audio.volume = volume / 100;
    audio.onloadedmetadata = () => {
      dispatch({
        type: PlayerActionTypes.SET_DURATION,
        payload: Math.ceil(audio.duration),
      });
    };
    audio.ontimeupdate = () => {
      dispatch({
        type: PlayerActionTypes.SET_CURRENT_TIME,
        payload: Math.ceil(audio.currentTime),
      });
    };
  };

  const playHandler = () => {
    if (pause) {
      dispatch({ type: PlayerActionTypes.PLAY });
      audio.play();
    } else {
      dispatch({ type: PlayerActionTypes.PAUSE });
      audio.pause();
    }
  };

  const volumeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    audio.volume = Number(e.target.value) / 100;
    dispatch({
      type: PlayerActionTypes.SET_VOLUME,
      payload: Number(e.target.value),
    });
  };

  const currentTimeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    audio.currentTime = Number(e.target.value);
    dispatch({
      type: PlayerActionTypes.SET_CURRENT_TIME,
      payload: Number(e.target.value),
    });
  };

  if (!active) {
    return null;
  }

  return (
    <div className={"player "}>
      <IconButton
        aria-label={"play"}
        icon={
          pause ? (
            <AiOutlinePlayCircle size="30px" color={"white"} />
          ) : (
            <AiOutlinePauseCircle size="30px" color={"white"} />
          )
        }
        size="lg"
        variant={"ghost"}
        _hover={{ bg: "none" }}
        className={"mr-6"}
        onClick={playHandler}
      />
      <div className={"mr-8"}>
        <div>
          <b>{active?.name}</b>
        </div>
        <div>{active?.artist}</div>
      </div>
      <TrackProgress
        left={currentTime}
        right={duration}
        onChange={currentTimeHandler}
      />
      <IconButton
        aria-label={"volume"}
        icon={<AiFillSound />}
        _hover={{ bg: "none" }}
        className={"ml-auto mr-4"}
        size="lg"
        variant={"ghost"}
      />
      <TrackProgress left={volume} right={100} onChange={volumeHandler} />
    </div>
  );
};

export default Player;
