import * as React from "react";
import {
  Button,
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerContent,
  DrawerOverlay,
  useDisclosure,
  Input,
  DrawerFooter,
  DrawerCloseButton,
  IconButton,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import Header from "./Header";

function Navbar() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef(null);

  return (
    <>
      <Header btnRef={btnRef} onOpen={onOpen} />
      <Drawer
        isOpen={isOpen}
        placement="end"
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerHeader>Create your account</DrawerHeader>

          <DrawerBody>
            <nav>
              {[
                { name: "Main", link: "/" },
                { name: "Albums", link: "/albums" },
                { name: "Tracks", link: "/tracks" },
                { name: "Liked", link: "/liked" },
              ].map((item, linkId) => (
                <Link key={linkId} to={item.link}>
                  <div onClick={onClose}>{item.name}</div>
                </Link>
              ))}
            </nav>
          </DrawerBody>

          <DrawerFooter>
            <Button variant="outline" mr={5} onClick={onClose}>
              Cancel
            </Button>
            <Button colorScheme="blue">Save</Button>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
}

export default Navbar;
