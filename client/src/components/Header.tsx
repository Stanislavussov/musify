import React, { RefObject } from "react";
import { IconButton } from "@chakra-ui/react";
import { HamburgerIcon } from "@chakra-ui/icons";
import { Link, useLocation } from "react-router-dom";

export const getHeaderText = (pathname: string) => {
  let text: null | JSX.Element | string = null;
  switch (true) {
    case pathname == "/tracks":
      text = "TRACK LIST";
      break;
    case pathname == "/albums":
      text = "ALBUMS";
      break;
    case pathname == "/liked":
      text = "MY FAVORITE";
      break;
  }

  return text;
};

const Header = ({
  btnRef,
  onOpen,
}: {
  btnRef: RefObject<HTMLButtonElement>;
  onOpen: () => void;
}) => {
  const location = useLocation();

  return (
    <div>
      <div className="bg-indigo-700 h-16 flex justify-between items-center p-10">
        <Link to={"/"} className="logo-text">
          Musify
        </Link>
        <IconButton
          ref={btnRef}
          icon={<HamburgerIcon />}
          aria-label={"menu-btn"}
          onClick={onOpen}
          colorScheme={"whiteAlpha"}
        />
      </div>
      <div className="header-title">{getHeaderText(location.pathname)}</div>
    </div>
  );
};

export default Header;
